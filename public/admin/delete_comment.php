<?php require_once("../../includes/initialize.php"); ?>
<?php if (!$session->is_logged_in()) { redirect_to("login.php"); } ?>
<?php
	
  if(empty($_GET['id'])) {
  	$session->message("Brak ID");
    redirect_to('index.php');
  }

  $comment = Comment::find_by_id($_GET['id']);
  if($comment && $comment->delete()) {
    $session->message("Komentarz zostal usuniety");
    redirect_to("comments.php?id={$comment->photograph_id}");
  } else {
    $session->message("Komentarz nie moze zostac usuniety");
    redirect_to('list_photos.php');
  }
  
?>
<?php if(isset($database)) { $database->close_connection(); } ?>
