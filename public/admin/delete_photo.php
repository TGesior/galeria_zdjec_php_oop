<?php require_once("../../includes/initialize.php"); ?>
<?php if (!$session->is_logged_in()) { redirect_to("login.php"); } ?>
<?php
	
  if(empty($_GET['id'])) {
  	$session->message("Brak ID");
    redirect_to('index.php');
  }

  $photo = Photograph::find_by_id($_GET['id']);
  if($photo && $photo->destroy()) {
    $session->message("Zdjecie {$photo->filename} zostalo usniete.");
    redirect_to('list_photos.php');
  } else {
    $session->message("Zdjecie nie moze zostac usuniete.");
    redirect_to('list_photos.php');
  }
  
?>
<?php if(isset($database)) { $database->close_connection(); } ?>
