<?php require_once("../includes/initialize.php"); ?>
<?php
  if(empty($_GET['id'])) {
    $session->message("Brak ID zdjecia.");
    redirect_to('index.php');
  }
  
  $photo = Photograph::find_by_id($_GET['id']);
  if(!$photo) {
    $session->message("NIE MOZNA ZNALESC ZDJECIA.");
    redirect_to('index.php');
  }

	if(isset($_POST['submit'])) {
	  $author = trim($_POST['author']);
	  $body = trim($_POST['body']);
	
	  $new_comment = Comment::make($photo->id, $author, $body);
	  if($new_comment && $new_comment->save()) {
			
			$new_comment->try_to_send_notification();
			
	   
	    redirect_to("photo.php?id={$photo->id}");
	
		} else {
			
	    $message = "NIE MOZNA ZAPISAC KOMENTARZA.";
		}
	} else {
		$author = "";
		$body = "";
	}
	
	$comments = $photo->comments();
	
?>
<?php include_layout_template('header.php'); ?>

<a href="index.php">&laquo; Powrot</a><br />
<br />

<div style="margin-left: 20px;">
  <img src="<?php echo $photo->image_path(); ?>" />
  <p><?php echo $photo->caption; ?></p>
</div>

<div id="comments">
  <?php foreach($comments as $comment): ?>
    <div class="comment" style="margin-bottom: 2em;">
	    <div class="author">
	      <?php echo htmlentities($comment->author); ?> napisal:
	    </div>
      <div class="body">
				<?php echo strip_tags($comment->body, '<strong><em><p>'); ?>
			</div>
	    <div class="meta-info" style="font-size: 0.8em;">
	      <?php echo datetime_to_text($comment->created); ?>
	    </div>
    </div>
  <?php endforeach; ?>
  <?php if(empty($comments)) { echo "Brak komentarzy."; } ?>
</div>

<div id="comment-form">
  <h3>Nowy komentarz</h3>
  <?php echo output_message($message); ?>
  <form action="photo.php?id=<?php echo $photo->id; ?>" method="post">
    <table>
      <tr>
        <td>Twoje imie:</td>
        <td><input type="text" name="author" value="<?php echo $author; ?>" /></td>
      </tr>
      <tr>
        <td>Twoj komentarz:</td>
        <td><textarea name="body" cols="40" rows="8"><?php echo $body; ?></textarea></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><input type="submit" name="submit" value="Wyslij komentarz" /></td>
      </tr>
    </table>
  </form>
</div>

<?php include_layout_template('footer.php'); ?>
